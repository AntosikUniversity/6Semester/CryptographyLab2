﻿using System;
using System.Collections;
using System.Numerics;

namespace CryptographyLab2
{
    public static class Helpers
    {
        public static Random Random = new Random();

        public static int GCD(int a, int b)
        {
            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }

            return a == 0 ? b : a;
        }

        public static int LCM(int a, int b)
        {
            return Math.Abs(a * b) / GCD(a, b);
        }

        public static int ExtendedGCD(int a, int b, out int x, out int y)
        {
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }

            var d = ExtendedGCD(b % a, a, out var x1, out var y1);
            x = y1 - b / a * x1;
            y = x1;

            return d;
        }

        public static int mulinv(int e, int varphi)
        {
            var x = 0;
            var _ = 0;
            ExtendedGCD(e, varphi, out x, out _);

            return varphi + x % varphi;
        }

        // See https://en.wikipedia.org/wiki/Newton's_method#Square_root_of_a_number
        public static BigInteger SqrtBigInteger(BigInteger N)
        {
            var rootN = N;
            var bitLength = 1;
            while (rootN / 2 != 0)
            {
                rootN /= 2;
                bitLength++;
            }

            bitLength = (bitLength + 1) / 2;
            rootN = N >> bitLength;

            var lastRoot = BigInteger.Zero;
            do
            {
                lastRoot = rootN;
                rootN = (BigInteger.Divide(N, rootN) + rootN) >> 1;
            } while ((rootN ^ lastRoot).ToString() != "0");

            return rootN;
        }

        public static int BitArrayToInt(BitArray bitArray)
        {
            if (bitArray.Length > 32)
                throw new ArgumentException("Argument length shall be at most 32 bits.");

            var array = new int[1];
            bitArray.CopyTo(array, 0);
            return array[0];
        }

        public static long BitArrayToLong(BitArray bitArray)
        {
            if (bitArray.Length > 64)
                throw new ArgumentException("Argument length shall be at most 64 bits.");

            var array = new byte[8];
            bitArray.CopyTo(array, 0);
            return BitConverter.ToInt64(array, 0);
        }

        public static byte[] BitArrayToByteArray(BitArray bits)
        {
            var ret = new byte[(bits.Length - 1) / 8 + 1];
            bits.CopyTo(ret, 0);
            return ret;
        }
    }
}