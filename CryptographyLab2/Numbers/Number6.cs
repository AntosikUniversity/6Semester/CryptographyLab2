﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;

namespace CryptographyLab2.Numbers
{
    public static class Number6
    {
        public static void Run()
        {
            Console.Write("Введите целое число (a): ");
            var aStr = Console.ReadLine();
            if (!int.TryParse(aStr, out var aNum)) Console.WriteLine("Неверный ввод!");
            Console.Write("Введите целое число (b): ");
            var bStr = Console.ReadLine();
            if (!int.TryParse(bStr, out var bNum)) Console.WriteLine("Неверный ввод!");

            var a = new BigGalois(aNum);
            var b = new BigGalois(bNum);
            Console.WriteLine($"{a.Number} = {a}");
            Console.WriteLine($"{b.Number} = {b}");

            var sum = BigGalois.Sum(a, b);
            Console.WriteLine($"{a.Number} + {b.Number} = {sum.Number} = {sum}");

            var multi = BigGalois.Multiply(a, b);
            Console.WriteLine($"{a.Number} * {b.Number} = {multi.Number} = {multi}");
        }

        public class BigGalois
        {
            public BigGalois(BigInteger number)
            {
                Number = number;
            }

            public BigInteger Number { get; set; }

            public static BigGalois Sum(BigGalois a, BigGalois b)
            {
                return new BigGalois(a.Number ^ b.Number);
            }

            public static BigGalois Multiply(BigGalois a, BigGalois b)
            {
                var bitsA = a.GetBitArray();
                var bitsB = b.GetBitArray();

                var bitsResult = new BitArray(bitsA.Length * bitsB.Length);

                for (var i = bitsA.Length - 1; i >= 0; i--)
                {
                    for (var j = bitsB.Length - 1; j >= 0; j--)
                    {
                        if (bitsA[i] && bitsB[j])
                            bitsResult[i + j] ^= true;
                    }
                }

                var result = Helpers.BitArrayToByteArray(bitsResult);
                return new BigGalois(new BigInteger(result));
            }

            public override string ToString()
            {
                var bits = GetBitArray();
                var sb = new List<string>(bits.Length);

                for (var i = bits.Length - 1; i >= 0; i--)
                {
                    if (bits[i])
                        sb.Add($"x^{i}");
                }


                return string.Join(
                    " + ",
                    sb
                );
            }

            public BitArray GetBitArray()
            {
                var bytes = Number.ToByteArray();
                var result = new BitArray(bytes.Length * 8);

                for (var i = bytes.Length - 1; i >= 0; i--)
                {
                    var bits = new BitArray(new[] { bytes[i] });

                    for (var j = bits.Length - 1; j >= 0; j--)
                    {
                        result[i*8+j] = bits[j];
                    }

                }

                return result;
            }

            public static implicit operator BigInteger(BigGalois a)
            {
                return a.Number;
            }
        }
    }
}