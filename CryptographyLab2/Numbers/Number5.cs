﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CryptographyLab2.Numbers
{
    public static class Number5
    {
        public static void Run()
        {
            Console.Write("Введите целое число (a): ");
            var aStr = Console.ReadLine();
            if (!int.TryParse(aStr, out var aNum)) Console.WriteLine("Неверный ввод!");
            Console.Write("Введите целое число (b): ");
            var bStr = Console.ReadLine();
            if (!int.TryParse(bStr, out var bNum)) Console.WriteLine("Неверный ввод!");

            var a = new Galois(aNum);
            var b = new Galois(bNum);
            Console.WriteLine($"{a.Number} = {a}");
            Console.WriteLine($"{b.Number} = {b}");

            var sum = Galois.Sum(a, b);
            Console.WriteLine($"{a.Number} + {b.Number} = {sum.Number} = {sum}");

            var multi = Galois.Multiply(a, b);
            Console.WriteLine($"{a.Number} * {b.Number} = {multi.Number} = {multi}");
        }
    }

    public class Galois
    {
        public Galois(int number)
        {
            Number = number;
        }

        public int Number { get; set; }

        public static Galois Sum(Galois a, Galois b)
        {
            return new Galois(a.Number ^ b.Number);
        }

        public static Galois Multiply(Galois a, Galois b)
        {
            var bitsResult = new BitArray(64);
            var bitsA = new BitArray(new int[] {a});
            var bitsB = new BitArray(new int[] {b});

            for (var i = bitsA.Length - 1; i >= 0; i--)
            {
                for (var j = bitsB.Length - 1; j >= 0; j--)
                {
                    if (bitsA[i] && bitsB[j])
                        bitsResult[i + j] ^= true;
                }
            }

            var result = Helpers.BitArrayToLong(bitsResult);
            return new Galois((int) result);
        }

        public override string ToString()
        {
            var bits = new BitArray(new[] {Number});
            var sb = new List<string>(bits.Length);

            for (var i = bits.Length - 1; i >= 0; i--)
            {
                if (bits[i])
                    sb.Add($"x^{i}");
            }


            return string.Join(
                " + ",
                sb
            );
        }

        public static implicit operator int(Galois a)
        {
            return a.Number;
        }
    }
}