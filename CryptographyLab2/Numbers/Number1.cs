﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptographyLab2.Numbers
{
    public static class Number1
    {
        public static void Run()
        {
            Console.WriteLine("#1.1");
            Console.Write("Введите целое число: ");
            var numberStr1 = Console.ReadLine();
            if (!int.TryParse(numberStr1, out var number1)) Console.WriteLine("Неверный ввод!");

            var primesLess = PrimesLessThan(number1);
            Console.WriteLine($"Простые числа меньшие {number1}: {string.Join(", ", primesLess)}");

            Console.WriteLine();
            Console.WriteLine("#1.2");
            var numberStr2 = Console.ReadLine();
            if (!int.TryParse(numberStr2, out var number2)) Console.WriteLine("Неверный ввод!");

            var system = PrimesLessThan(number2).Where(number => Helpers.GCD(number, number2) == 1);
            Console.WriteLine($"Приведенная система вычетов по модулю {number2}: {string.Join(", ", system)}");

            Console.WriteLine();
            Console.WriteLine("#1.3");
            Console.Write("Введите целое число: ");
            var numberStr3 = Console.ReadLine();
            if (!int.TryParse(numberStr3, out var number3)) Console.WriteLine("Неверный ввод!");

            var phi = Phi(number3);
            Console.WriteLine($"Значение функции Phi({number3}) = {phi}");

            Console.WriteLine();
            Console.WriteLine("#1.4");
            Console.Write("Введите целое число: ");
            var numberStr4 = Console.ReadLine();
            if (!int.TryParse(numberStr4, out var number4)) Console.WriteLine("Неверный ввод!");

            var decomposition = GetPrimesDecomposition(number4);
            var polynom = PolynomFromPrimes(decomposition);
            Console.WriteLine($"{number4} в виде полинома простых чисел: {polynom}");
        }

        #region 1.1

        public static IEnumerable<int> PrimesLessThan(int number)
        {
            // see https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
            var list = Enumerable.Repeat(true, number).ToList();
            list[0] = false;
            list[1] = true;

            for (var i = 2; i < number; i++)
            {
                if (!list[i]) continue;

                var pow = (int) Math.Pow(i, 2);
                for (var j = pow; j < number; j += i) list[j] = false;
            }

            return list
                .Select((value, index) => new {value, index})
                .Where(el => el.value)
                .Select(el => el.index);
        }

        #endregion // 1.1

        #region 1.3

        public static int Phi(int m)
        {
            var result = m;

            for (var i = 2; i * i <= m; ++i)
            {
                if (m % i == 0)
                {
                    while (m % i == 0)
                    {
                        m /= i;
                    }

                    result -= result / i;
                }

                if (m > 1) result -= result / m;
            }

            return result;
        }

        #endregion // 1.3

        #region 1.4

        public static string PolynomFromPrimes(IEnumerable<int> primes)
        {
            return string.Join(
                " + ",
                primes
                    .GroupBy(el => el)
                    .Select(el => string.Format("{1}x^{0}", el.Key, el.Count()))
            );
        }

        public static IEnumerable<int> GetPrimesDecomposition(decimal n)
        {
            var storage = new List<int>();
            while (n > 1)
            {
                var i = 1;
                while (true)
                {
                    if (IsPrime(i))
                        if (n / i == Math.Round(n / i))
                        {
                            n /= i;
                            storage.Add(i);
                            break;
                        }

                    i++;
                }
            }

            return storage;
        }

        public static bool IsPrime(int n)
        {
            if (n <= 1) return false;
            for (var i = 2; i <= Math.Sqrt(n); i++)
            {
                if (n % i == 0)
                    return false;
            }

            return true;
        }

        #endregion // 1.4
    }
}