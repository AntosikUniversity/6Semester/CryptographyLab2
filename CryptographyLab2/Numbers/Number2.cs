﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace CryptographyLab2.Numbers
{
    public static class Number2
    {
        public static void Run()
        {
            Console.WriteLine("ElGamal: ");
            var keyElGamal = ElGamal.GenerateKey(out var xElGamal);
            BigInteger messageElGamal = Helpers.Random.Next(100, 1000);

            Console.WriteLine($"Keys: private - {xElGamal}, public - {keyElGamal}");
            Console.WriteLine($"Message: {messageElGamal}");

            var encryptedElGamal = ElGamal.EncryptMessage(messageElGamal, keyElGamal);
            Console.WriteLine($"Encrypted message: {encryptedElGamal}");

            var decryptedElGamal = ElGamal.DecryptMessage(encryptedElGamal, keyElGamal, xElGamal);
            Console.WriteLine($"Decrypted message: {decryptedElGamal}");

            Console.WriteLine();
            Console.WriteLine("Rabin: ");
            var keyRabin = Rabin.GenerateKey(out var xRabin);
            BigInteger messageRabin = Helpers.Random.Next(100, 1000);

            Console.WriteLine($"Keys: private - {keyRabin}, public - {xRabin}");
            Console.WriteLine($"Message: {messageRabin}");

            var encryptedRabin = Rabin.EncryptMessage(messageRabin, keyRabin);
            Console.WriteLine($"Encrypted message: {encryptedRabin}");

            var decryptedRabin = Rabin.DecryptMessage(encryptedRabin, keyRabin, xRabin);
            Console.WriteLine($"Decrypted message: ({string.Join(", ", decryptedRabin)})");
        }
    }

    // See https://ru.wikipedia.org/wiki/%D0%A1%D1%85%D0%B5%D0%BC%D0%B0_%D0%AD%D0%BB%D1%8C-%D0%93%D0%B0%D0%BC%D0%B0%D0%BB%D1%8F#%D0%9F%D1%80%D0%B8%D0%BC%D0%B5%D1%80
    public static class ElGamal
    {
        public static OpenKey GenerateKey(out BigInteger privateKey)
        {
            var p = GetRandomPrime();
            var g = p - 1;
            var x = Helpers.Random.Next(p - 1);

            var pBig = new BigInteger(p);
            var gBig = new BigInteger(g);
            var xBig = new BigInteger(x);

            var yBig = BigInteger.ModPow(gBig, xBig, pBig);

            privateKey = xBig;

            return new OpenKey(pBig, gBig, yBig);
        }

        public static EncryptedText EncryptMessage(BigInteger message, OpenKey openKey)
        {
            var k = Helpers.Random.Next((int) openKey.p - 1);
            var aBig = BigInteger.ModPow(openKey.g, k, openKey.p);
            var bBig = BigInteger.ModPow(BigInteger.Pow(openKey.y, k) * message, 1, openKey.p);

            return new EncryptedText(aBig, bBig);
        }

        public static BigInteger DecryptMessage(EncryptedText encryptedMessage, OpenKey openKey, BigInteger privateKey)
        {
            return BigInteger.ModPow(
                encryptedMessage.b * BigInteger.Pow(
                    encryptedMessage.a,
                    (int) (openKey.p - 1 - privateKey)
                ),
                1,
                openKey.p);
        }

        private static int GetRandomPrime(int range = 1000)
        {
            var primes = Number1.PrimesLessThan(range).ToList();
            var randomIndex = Helpers.Random.Next(10, primes.Count);

            return primes[randomIndex];
        }

        public struct OpenKey
        {
            public OpenKey(BigInteger p, BigInteger g, BigInteger y)
            {
                this.p = p;
                this.g = g;
                this.y = y;
            }

            public BigInteger p;
            public BigInteger g;
            public BigInteger y;

            public override string ToString()
            {
                return $"({p}; {g}; {y})";
            }
        }

        public struct EncryptedText
        {
            public EncryptedText(BigInteger a, BigInteger b)
            {
                this.a = a;
                this.b = b;
            }

            public BigInteger a;
            public BigInteger b;

            public override string ToString()
            {
                return $"({a}, {b})";
            }
        }
    }

    // See https://ru.wikipedia.org/wiki/%D0%9A%D1%80%D0%B8%D0%BF%D1%82%D0%BE%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0_%D0%A0%D0%B0%D0%B1%D0%B8%D0%BD%D0%B0
    public static class Rabin
    {
        public static BigInteger GenerateKey(out PrivateKey privateKey)
        {
            var p = GetRandomPrime(10000);
            var q = GetRandomPrime(10000);

            var n = p * q;

            var pBig = new BigInteger(p);
            var qBig = new BigInteger(q);
            var nBig = new BigInteger(n);

            privateKey = new PrivateKey(pBig, qBig);

            return nBig;
        }

        public static BigInteger EncryptMessage(BigInteger message, BigInteger openKey)
        {
            return BigInteger.ModPow(message, 2, openKey);
        }

        public static IEnumerable<BigInteger> DecryptMessage(BigInteger encryptedMessage, BigInteger openKey,
            PrivateKey privateKey)
        {
            var sqrtMsg = Helpers.SqrtBigInteger(encryptedMessage);
            var m_p = BigInteger.ModPow(sqrtMsg, 1, privateKey.p);
            var m_q = BigInteger.ModPow(sqrtMsg, 1, privateKey.q);
            
            Helpers.ExtendedGCD((int) privateKey.p, (int) privateKey.q, out var y_p, out var y_q);
            
            var r = BigInteger.Abs(BigInteger.ModPow(
                y_p * privateKey.p * m_q + y_q * privateKey.q * m_p,
                1,
                openKey
            ));
            var s = BigInteger.Abs(BigInteger.ModPow(
                y_p * privateKey.p * m_q - y_q * privateKey.q * m_p,
                1,
                openKey
            ));

            return new List<BigInteger> {r, openKey - r, s, openKey - s}
                .Select(number => BigInteger.ModPow(number, 1, openKey));
        }

        private static int GetRandomPrime(int range = 1000)
        {
            var primes = Number1.PrimesLessThan(range).ToList();
            var randomIndex = Helpers.Random.Next(10, primes.Count);

            return primes[randomIndex];
        }

        public struct PrivateKey
        {
            public PrivateKey(BigInteger p, BigInteger q)
            {
                this.p = p;
                this.q = q;
            }

            public BigInteger p;
            public BigInteger q;

            public override string ToString()
            {
                return $"({p}; {q})";
            }
        }
    }
}