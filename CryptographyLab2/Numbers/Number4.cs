﻿using System;
using System.Linq;
using System.Numerics;

namespace CryptographyLab2.Numbers
{
    public static class Number4
    {
        public static void Run()
        {
            Console.WriteLine("RSA: ");
            var publicKey = RSA.GenerateKey(out var privateKey);
            BigInteger message = Helpers.Random.Next(100);

            Console.WriteLine($"Keys: private - {privateKey}, public - {publicKey}");
            Console.WriteLine($"Message: {message}");

            var encrypted = RSA.EncryptMessage(message, publicKey);
            Console.WriteLine($"Encrypted message: {encrypted}");

            var decrypted = RSA.DecryptMessage(encrypted, privateKey);
            Console.WriteLine($"Decrypted message: {decrypted}");
        }
    }

    public static class RSA
    {
        public static Random Random = new Random();
        public static int e = 65537;

        public static PublicKey GenerateKey(out PrivateKey privateKey)
        {
            var p = GetRandomPrime();
            var q = GetRandomPrime();

            var n = p * q;
            var varphi = (p - 1) * (q - 1);
            var d = varphi + Helpers.mulinv(e, varphi);

            privateKey = new PrivateKey(d, n);

            return new PublicKey(e, n);
        }

        public static BigInteger EncryptMessage(BigInteger message, PublicKey publicKey)
        {
            return BigInteger.ModPow(message, e, publicKey.n);
        }

        public static BigInteger DecryptMessage(BigInteger encryptedMessage, PrivateKey privateKey)
        {
            return BigInteger.ModPow(encryptedMessage, privateKey.d, privateKey.n);
        }

        private static int GetRandomPrime(int range = 1000)
        {
            var primes = Number1.PrimesLessThan(range).ToList();
            var randomIndex = Random.Next(10, primes.Count);

            return primes[randomIndex];
        }
        
        public struct PublicKey
        {
            public PublicKey(BigInteger e, BigInteger n)
            {
                this.e = e;
                this.n = n;
            }

            public BigInteger e;
            public BigInteger n;

            public override string ToString()
            {
                return $"({e}; {n})";
            }
        }

        public struct PrivateKey
        {
            public PrivateKey(BigInteger d, BigInteger n)
            {
                this.d = d;
                this.n = n;
            }

            public BigInteger d;
            public BigInteger n;

            public override string ToString()
            {
                return $"({d}; {n})";
            }
        }
    }
}