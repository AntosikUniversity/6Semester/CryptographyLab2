﻿using System;
using System.Numerics;

namespace CryptographyLab2.Numbers
{
    public static class Number3
    {
        // See https://en.wikipedia.org/wiki/Modular_exponentiation
        public static void Run()
        {
            Console.Write("Введите целое число: ");
            var numberStr = Console.ReadLine();
            if (!BigInteger.TryParse(numberStr, out var number)) Console.WriteLine("Неверный ввод!");

            Console.Write("Введите целое число (экспонента): ");
            var eStr = Console.ReadLine();
            if (!BigInteger.TryParse(eStr, out var e)) Console.WriteLine("Неверный ввод!");

            Console.Write("Введите целое число (модуль): ");
            var modulusStr = Console.ReadLine();
            if (!BigInteger.TryParse(modulusStr, out var modulus)) Console.WriteLine("Неверный ввод!");

            var modPow = ModPow(number, e, modulus);
            Console.WriteLine($"{number}^{e} mod {number} = {modPow}");
        }

        public static BigInteger ModPow(BigInteger number, BigInteger e, BigInteger modulus)
        {
            if (modulus == 1) return 0;

            BigInteger c = 1;

            for (var i = 1; i <= e; i++)
            {
                c = (c * number) % modulus;
            }

            return c;
        }
    }
}